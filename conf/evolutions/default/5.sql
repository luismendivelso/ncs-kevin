# Se cambia nombre de la columna artista a artista_id

# --- !Ups

ALTER TABLE cancion RENAME COLUMN artista TO artista_id;

# --- !Downs

ALTER TABLE cancion RENAME COLUMN artista_id TO artista;