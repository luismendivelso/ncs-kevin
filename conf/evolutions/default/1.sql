# Esquema artista

# --- !Ups

CREATE TABLE Artista (
  id SERIAL NOT NULL,
  email varchar(255) NOT NULL,
  usuario VARCHAR(255) NOT NULL,
  contrasena varchar(64) NOT NULL,
  nombre varchar(255) NOT NULL,
  ruta_imagen VARCHAR(255),
  CONSTRAINT pk_artista PRIMARY KEY (id)
);

# --- !Downs

DROP TABLE Artista;