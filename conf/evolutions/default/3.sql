# Esquema cancion

# --- !Ups

CREATE TABLE IF NOT EXISTS cancion(
  id SERIAL NOT NULL,
  nombre VARCHAR(64) NOT NULL ,
  ruta_archivo VARCHAR(255) NOT NULL ,
  artista BIGINT,
  CONSTRAINT pk_cancion PRIMARY KEY (id),
  CONSTRAINT fk_cancion_artista FOREIGN KEY (artista) REFERENCES artista(id)
);

# --- !Downs

DROP TABLE cancion;