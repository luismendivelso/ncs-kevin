# Se cambia el tipo de dato de la columna a integer

# --- !Ups

ALTER TABLE cancion ALTER COLUMN artista_id TYPE INTEGER;

# --- !Downs

ALTER TABLE cancion ALTER COLUMN artista_id TYPE BIGINT;