# Se quita restriccion not null a columna ruta archivo

# --- !Ups

ALTER TABLE cancion ALTER COLUMN ruta_archivo DROP NOT NULL;

# --- !Downs

ALTER TABLE cancion ALTER COLUMN ruta_archivo SET NOT NULL;