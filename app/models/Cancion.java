package models;

import play.data.validation.Constraints;

import javax.persistence.*;

/**
 * Created by kecc on 15/07/17.
 */
@Entity
public class Cancion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    @Constraints.Required
    private String nombre;

    @Column(name = "ruta_archivo")
    private String rutaArchivo;

    @ManyToOne
    private Artista artista;

    public Cancion() {
    }

    public Cancion(String nombre, String rutaArchivo, Artista artista) {
        this.nombre = nombre;
        this.rutaArchivo = rutaArchivo;
        this.artista = artista;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRutaArchivo() {
        return rutaArchivo;
    }

    public void setRutaArchivo(String rutaArchivo) {
        this.rutaArchivo = rutaArchivo;
    }

    public Artista getArtista() {
        return artista;
    }

    public void setArtista(Artista artista) {
        this.artista = artista;
    }
}
