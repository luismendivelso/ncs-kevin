package models;

import play.db.jpa.JPAApi;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

/**
 * Created by kecc on 15/07/17.
 */
public class CancionRepository extends AbstractRepository<Cancion> {

    @Inject
    public CancionRepository(JPAApi jpaApi, DatabaseExecutionContext ec){
        super(jpaApi, ec);
    }

    @Override
    public CompletionStage<Optional<Cancion>> get(Cancion data) {
        return null;
    }

    @Override
    public CompletionStage<Optional<Cancion>> update(Cancion newData) {
        return null;
    }

    @Override
    protected Stream<Cancion> list(EntityManager em) {
        return em.createQuery("select c from Cancion c", Cancion.class).getResultList().stream();
    }

    public CompletionStage<Optional<Cancion>> updateRutaArchivo(Cancion cancion){
        return CompletableFuture.supplyAsync(() -> wrap(em -> lookupRutaArchivo(em, cancion)));
    }

    private Optional<Cancion> lookupRutaArchivo(EntityManager em, Cancion cancion){
        Cancion data = em.find(Cancion.class, cancion.getId());
        data.setRutaArchivo(cancion.getRutaArchivo());
        return Optional.ofNullable(data);
    }

    @Override
    protected Cancion delete(EntityManager em, Cancion delete) {
        Cancion c = em.find(Cancion.class, delete.getId());
        em.remove(c);
        return c;
    }

    @Override
    protected Optional<Cancion> modify(EntityManager em, Cancion newData) {
        //TODO:
        return null;
    }
}
