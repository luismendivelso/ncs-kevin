package controllers;

import models.Artista;
import models.CancionRepository;
import play.data.FormFactory;
import play.mvc.*;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

/**
 * Controlador que contiene las acciones para la pagina de inicio y registro
 */
public class HomeController extends Controller {



    private final FormFactory formFactory;
    private final CancionRepository cancionRepository;

    /**
     * Constructor - Crea instancia de HomeController mediante la inyeccion de dependencias
     * @param formFactory Fabrica de formularios
     * @param cancionRepository Repositorio de canciones
     */
    @Inject
    public HomeController(FormFactory formFactory, CancionRepository cancionRepository){
        this.formFactory = formFactory;
        this.cancionRepository = cancionRepository;
    }

    /**
     * Visualiza la pagina de inicio, con la lista de canciones
     * @return CompletionStage de la pagina de inicio
     */
    public CompletionStage<Result> index() {
        return cancionRepository.list().thenApplyAsync(cancionStream -> ok(views.html.index.render(cancionStream.collect(Collectors.toList()))));
    }

    /**
     * Retorna la pagina de registro del artista
     * @return Resultado de la pagina de registro del artista
     */
    public Result signup() {
        return ok(views.html.signup.render(formFactory.form(Artista.class)));
    }

}
