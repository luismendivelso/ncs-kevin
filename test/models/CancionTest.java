package models;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kecc on 16/07/17.
 */
public class CancionTest {
    @Test
    public void testCancion(){
        Artista a = new Artista();
        a.setNombre("artista");
        Cancion c = new Cancion("test", "archivo", a);
        Assert.assertEquals("El nombre de la cancion no es el esperado", c.getNombre(), "test");
        Assert.assertEquals("La ruta del archivo no es la esperada", c.getRutaArchivo(), "archivo");
        Assert.assertNotNull("El artista no debe ser nulo", a);
        Assert.assertEquals("El artista no es el esperado", c.getArtista().getNombre(), "artista");
    }
}
