package models;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by kecc on 16/07/17.
 */
public class ArtistaTest {

    @Test
    public void testArtista(){
        Artista a = new Artista("test@test.com", "test", "test", "test");
        Assert.assertEquals("El email no es el esperado", "test@test.com", a.getEmail());
        Assert.assertEquals("El nombre del artista no es el esperado", "test", a.getNombre());
        Assert.assertEquals("El usuario del artista no es el esperado", "test", a.getUsuario());
        Assert.assertEquals("La contrasena no es la esperada", "test", a.getContrasena());
    }

}
