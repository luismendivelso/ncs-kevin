name := """ncsongs"""
organization := "com.kecc"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"

libraryDependencies += guice

herokuAppName in Compile := "lit-oasis-37882"

herokuConfigVars in Compile := Map(
  "APPLICATION_SECRET" -> "MyS3cr3tK3y",
  "SONGS_PATH" -> "/app/songs"
)

herokuIncludePaths in Compile := Seq(
  "public"
)

libraryDependencies ++= Seq(
  javaJpa,
  evolutions,
  "org.postgresql" % "postgresql" % "9.4.1212",
  "org.hibernate" % "hibernate-entitymanager" % "5.1.0.Final",
  "com.cloudinary" % "cloudinary-http44" % "1.13.0"
)

PlayKeys.externalizeResources := false